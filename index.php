<?php require 'consulta.php'; ?>

<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>
</head>
<body>
<?php foreach($consulta as $consultas){?>
<div class="row">
    <div class="col-12 col-lg-8 offset-lg-2">
        <div class="card card-plantao">
            <div class="card-header">
                <h3><?=$consultas['NOME_HC'];?></h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-5 col-lg-3">
                        <p class="card-title"><i class="far fa-calendar-alt"></i> <?=date('d/m/Y',strtotime($consultas['DATA_PLANTAO']));?></p>
                    </div>
                    <div class="col-7 col-lg-6">
                        <p class="card-title"><i class="far fa-clock"></i> <?=$consultas["HORA_IN_PLANTAO"];?> às <?=$consultas['HORA_FIN_PLANTAO'];?></p>
                    </div>
                </div>
                <p class="card-text">Hospital <?=$consultas['NOME_HC'];?>, precisa de <?=$consultas['QUANTIDADE_MED_PLANTAO'];?> de médico(s) <?=$consultas['ESPECIALIDADE'];?>.<br>
                <strong>Local:</strong><br>
                    <?=$consultas['LOGRADOURO'];?>, <?=$consultas['NUMERO'];?> - <?=$consultas['CIDADE'];?> - <?=$consultas['UF'];?><br>
                    CEP: <?=$consultas['CEP'];?>
                </p>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-1 offset-8">
                        <a href="">
                            <i class="far fa-check-circle fa-2x"></i>
                        </a>
                    </div>
                    <div class="col-1">
                        <a href="">
                            <i class="far fa-times-circle fa-2x"></i>
                        </a>
                    </div>
                    <div class="col-1">
                        <a href="http://maps.google.com/maps?=start&daddr=<?=$consultas['LOGRADOURO'];?>, <?=$consultas['NUMERO'];?> - <?=$consultas['CIDADE'];?> - <?=$consultas['UF'];?>, <?=$consultas['CEP'];?>"
                            <i class="fas fa-map-marker-alt fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>