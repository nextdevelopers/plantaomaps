$(document).ready(function(){
    var geocoder, map;
    var mouseMapEnable = true;
    function initialize() {

        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': 'Praa Cndido Dias Castejn, 116, Centro, So Jos dos Campos, SP, 12210-110, BRASIL'
        }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map = new google.maps.Map(document.getElementById('map-canvas'), {
                    zoom: 13,
                    center: results[0].geometry.location,
                    scrollwheel: false,
                    draggable: mouseMapEnable,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    }
                });

                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    animation: google.maps.Animation.DROP
                });
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
});

